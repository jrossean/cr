<?php
//transactions.php MJD
//this script if for adding, modifing and delete transactions
include "common.php";
require_once "includes/functions.inc.php";
require_once "includes/meekrodb.2.3.class.php";

$userid = $_COOKIE['USERID'];
$post = $_POST;
$date = date("Y-m-d H:i:s");

function sqldate ($dayenter){
  $daycheck = strpos("$dayenter","-");
  if ($daycheck >= "1") {
    $dayenter=str_replace("-", "/", "$dayenter");
    $dayenter= date_create("$dayenter");
//    echo date_format($dayenter,"Y-m-d");
//    echo "testing this true";
  }
  else {
    $dayenter=date_create("$dayenter");
//    echo date_format($dayenter,"Y-m-d");
//    echo "tests is false";
  }
}

if (isset($_REQUEST['action']))
{
  $action = $_REQUEST['action'];
  if ($action == "Add Transaction")
  {
    if ($post['transtypeid']=="1" or $post['transtypeid']=="3" or $post['transtypeid']=="4")
      $amount = abs($post['amount']) * -1;
    else
      $amount = abs($post['amount']);

//      sqldate ($post['transdate']);
      DB::insert("transactions", array(
        "transtypeid" => $post['transtypeid'],
        "description" => $post['description'],
        "amount" => $amount,
        "dateadded" => $date,
        "accountid" => $post['accountid'],
        "vendorid" => $post['vendorid'],
        "cattype" => $post['cattypeid'],
        "date" => $post['transdate'],
//        "date" => $dayenter,
        "userid" => $userid));

    header("Location: transactions.php");
    exit;
  }
  else if ($action == "Update Transaction")
  {
      if ($post['transtypeid']=="1" or $post['transtypeid']=="3" or $post['transtypeid']=="4")
        $amount = abs($post['amount']) * -1;
      else
        $amount = abs($post['amount']);

      DB::update("transactions",array(
        "transtypeid" => $post['transtypeid'],
        "description" => $post['description'],
        "amount" => $amount,
        "dateadded" => $date,
        "accountid" => $post['accountid'],
        "vendorid" => $post['vendorid'],
        "cattype" => $post['cattypeid'],
        "date" => $post['transdate'],
        ),"id=%i",$post['transid']);
  
      header("Location: transactions.php");
      exit;
  }
}

if (isset($_REQUEST['option']))
  $option = $_REQUEST['option'];
else
  $option = "viewtransactions";

startPage("Transactions");
//addField($type, $labelfor, $label, $name, $placeholder, $options = NULL, $value = NULL)
//DB::debugMode();
if (isset($option))
{
    if ($option == "addtrans" OR $option == "edittrans")
    {

        $cattype = db::query("select id, name from categorytype where (userid is null or userid = %i) order by name",$userid);
        $vendor  = db::query("select id, name from vendorinfo where (userid is null or userid = %i) order by name",$userid);
        $accountid = db::query("select id, accountname name from accounts where (userid = %i)",$userid);
        $transtype  = db::query("select id, name from transactiontype");

        if ($option == "addtrans")
          $title = "Add Transaction";
        else if ($option == "edittrans")
        {
          $title = "Edit Transaction";
          $trans = DB::queryFirstRow("SELECT * FROM transactions WHERE id = %i",$_REQUEST['transid']);
        }

        $transform = new FormCreate;
        $transform->startForm("transactions.php",$title);
        $transform->addField("select","accountid","Account","accountid", "Select account",$accountid,$trans['accountid']);
        $transform->addField("text","transdate","Transaction Date","transdate","YYYY-MM-DD",null,$trans['date']);
        $transform->addField("select","transtype","Transaction Type","transtypeid",null,$transtype,$trans['transtypeid']);
        $transform->addField("select","vendorid","Who paid","vendorid",null,$vendor,$trans['vendorid']);
        $transform->addField("select","cattypeid","Category","cattypeid",null,$cattype,$trans['cattypeid']);
        $transform->addField("text","amount","Amount","amount",null,null,$trans['amount']);
        $transform->addField("text","description","Description","description",null,null,$trans['description']);

        if ($option == "addtrans")
            $transform->addField("button",NULL,NULL,"Add Transaction",NULL,NULL,NULL);
        else if ($option == "edittrans")
        {
            $transform->addField("button",NULL,NULL,"Update Transaction",NULL,NULL,NULL);
            $transform->addField("hidden",NULL,NULL,"transid",NULL,NULL,$trans['id']);
        }
        $transform->endForm();
    }
    else if ($option == "viewtransactions")
    {
        $transactions = DB::query("SELECT transactions.id, transactiontype.name type, transactions.description, abs(transactions.amount) amount, accounts.accountname, vendorinfo.name vendorname, categorytype.name cattypename, transactions.date
                                      FROM transactions, transactiontype, accounts, vendorinfo, categorytype
                                      WHERE transactions.`transtypeid` = transactiontype.id
                                      AND transactions.accountid = accounts.id
                                      AND transactions.vendorid = vendorinfo.id
                                      AND transactions.cattype = categorytype.id
                                      AND transactions.userid = %i
                                      ORDER BY date",$userid);
?>
<div class="col-md=12">
  <a href="transactions.php?option=addtrans" class="btn btn-primary">Add Transactions</a>

</div>
<div class="col-md-12">

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Type</th>
            <th>Description</th>
            <th>Account</th>
            <th>Vendor</th>
            <th>Category</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Action</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Type</th>
            <th>Description</th>
            <th>Account</th>
            <th>Vendor</th>
            <th>Category</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Action</th>
          </tr>
        </tfoot>
        <tbody>
<?php
foreach ($transactions AS $trans)
{
?>
          <tr>
            <td><?php echo $trans['type']; ?></td>
            <td><?php echo $trans['description']; ?></td>
            <td><?php echo $trans['accountname']; ?></td>
            <td><?php echo $trans['vendorname']; ?></td>
            <td><?php echo $trans['cattypename']; ?></td>
            <td><?php echo $trans['date']; ?></td>
            <td style="text-align: right; padding-right: 5px;">$<?php echo number_format(abs($trans['amount']), 2); ?></td>
            <td><a href="?option=edittrans&transid=<?php echo $trans['id']; ?>" class="btn btn-primary">Edit</a></td>
          </tr>
<?php  
}
?>
        </tbody>
      </table>

<?php      
    }
}

endPage();
?>