<?php
// index.php
//include "common.php";
include "includes/meekrodb.2.3.class.php";

if (isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
	if ($action == "Sign In")
	{
		$user = DB::queryFirstRow("SELECT id, email, passwd FROM users WHERE active = 1 AND email = %s",$_REQUEST['username']);
		$count = DB::count();

		if ($count == 1)
		{
			if ($user['passwd'] === md5($_REQUEST['password']))
			{
				setcookie("USERID",$user['id']);
				//header("Location: dashboard.php");
        header("Location: accounts.php");
				exit();
			}
			else
			{
				header("Location: index.php?error=loginfail");
				exit();
			}
		  
    }
    else {
        header("Location: index.php?error=loginfail");
        exit();
      } 

		//exit();


	}
}

// This script is for the login form. Once logged in, they should be forwarded to the dashboard.php page.

?>
<!doctype html>
<html lang="en">
<head>
<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
<style type="text/css">
  body {
    padding-top: 120px;
    padding-bottom: 40px;
    background-color: #eee;
  
  }
  .btn 
  {
   outline:0;
   border:none;
   border-top:none;
   border-bottom:none;
   border-left:none;
   border-right:none;
   box-shadow:inset 2px -3px rgba(0,0,0,0.15);
  }
  .btn:focus
  {
   outline:0;
   -webkit-outline:0;
   -moz-outline:0;
  }
  .fullscreen_bg {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-size: cover;
    background-position: 50% 50%;
    background-image: url('http://dental.jaredrossean.com/img/teethbg.png');
    background-repeat:repeat;
  }
  .form-signin {
    max-width: 280px;
    padding: 15px;
    margin: 0 auto;
      margin-top:50px;
  }
  .form-signin .form-signin-heading, .form-signin {
    margin-bottom: 10px;
  }
  .form-signin .form-control {
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }
  .form-signin .form-control:focus {
    z-index: 2;
  }
  .form-signin input[type="text"] {
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    border-top-style: solid;
    border-right-style: solid;
    border-bottom-style: none;
    border-left-style: solid;
    border-color: #000;
  }
  .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-top-style: none;
    border-right-style: solid;
    border-bottom-style: solid;
    border-left-style: solid;
    border-color: rgb(0,0,0);
    border-top:1px solid rgba(0,0,0,0.08);
  }
  .form-signin-heading {
    color: #fff;
    text-align: center;
    text-shadow: 0 2px 2px rgba(0,0,0,0.5);
  }
</style>
</head>
<body>
<div id="fullscreen_bg" class="fullscreen_bg" />

<div class="container">

    <form class="form-signin" method="post" action="index.php">
		<h1 class="form-signin-heading text-muted">Sign In</h1>
		<input type="text" class="form-control" placeholder="Email address" name="username" required="" autofocus="">
		<input type="password" class="form-control" placeholder="Password" name="password" required="">
		<input type="submit" name="action" value="Sign In" class="btn btn-lg btn-primary btn-block" />
	</form>

</div>
<div class="col-md-12" id="errorbox">
    <?php
    if(isset ($_GET['error']) and $_GET['error']=="loginfail")
      echo "User name and/or password don't match";
    ?>
</div>
</body>
</html>
