<?php
// accounts.php by MJD
// This script is for adding, modifing and deactivating accounts
// include "index.php"; 
include "common.php";
require_once "includes/functions.inc.php";
require_once "includes/meekrodb.2.3.class.php";
$userid = $_COOKIE['USERID'];

if (isset($_REQUEST['option']))
  $option = $_REQUEST['option'];
else
  $option = "viewaccounts";

if (isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
	if ($action == "Add Account")
	{
    $post = $_POST;
    $date = date("Y-m-d H:i:s");
    DB::insert("accounts", array(
      "accountname" => $post['accountname'],
      "accounttype" => $post['accounttype'],
      "starting" => $post['starting'],
      "dateadded" => $date,
      "userid" => $userid));
    header("Location: accounts.php");
  } 
  else if ($action == "Update Account")
  {
      $post = $_POST;

      DB::update("accounts",array(
        "accountname" => $post['accountname'],
        "accounttype" => $post['accounttype'],
        ),"id=%i",$post['accountid']);

  }
  else if ($action == "Deactivate Account")
  {
      $accountid = $_REQUEST['accountid'];
      DB::update("accounts",array(
        "active" => 0
        ),"id=%i",$accountid);
  }
}
startPage("Accounts");

if (isset($option))
{
  if ($option == "addaccount" OR $option == "editaccount")
  {

      //addField($type, $labelfor, $label, $name, $placeholder, $options = NULL, $value = NULL)
      $types = array(
                  array("id" => "Checking", "name" => "Checking"),
                  array("id" => "Savings", "name" => "Savings"),
                  array("id" => "Credit", "name" => "Credit")
                  );
      if ($option == "addaccount")
      {
          $title = "Add Account";
      }
      else if ($option == "editaccount")
      {
          $title = "Edit Account";
          $account = DB::queryFirstRow("SELECT * FROM accounts WHERE id = %i",$_REQUEST['accountid']);
      }
      $accountform = new FormCreate;
      $accountform->startForm("accounts.php",$title);
      $accountform->addField("text","accountname","Account Name","accountname","Please enter name for account",NULL,$account['accountname']);
      $accountform->addField("select","accounttype","Type","accounttype",NULL,$types,$account['accounttype']);
      $accountform->addField("text","starting","Starting Balance","starting","Enter starting balance",NULL,$account['starting']);
      
      if ($option == "addaccount")
      {
          $accountform->addField("button",NULL,NULL,"Add Account",NULL,NULL,NULL);
      }
      else if ($option == "editaccount")
      {
          $accountform->addField("button",NULL,NULL,"Update Account",NULL,NULL,NULL);
          $accountform->addField("hidden",NULL,NULL,"accountid",NULL,NULL,$_REQUEST['accountid']);
      }
      $accountform->endForm();
  }
  else if ($option == "viewaccounts")
  {
?>
<div class="col-md=12">
  <a href="accounts.php?option=addaccount" class="btn btn-primary">Add Account</a>
</div>
<div class="col-md-12">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Balance</th>
        <th>Action</th>
      </tr>
    </thead>
    <tfoot>
    </tfoot>
    <tbody>
<?php
$accounts = DB::query("SELECT * FROM accounts WHERE userid = %i AND active = 1",$userid);
foreach ($accounts AS $account)
{
?>
      <tr>
        <td><?php echo $account['accountname']; ?></td>
        <td><?php echo $account['accounttype']; ?></td>
        <td><?php echo $account['starting']; ?></td>
        <td><a href="accounts.php?option=editaccount&accountid=<?php echo $account['id']; ?>" class="btn btn-primary">Edit</a></td>
      </tr>
<?php
}
?>
    </tbody>
  </table>
</div>
<?php
  }
}

endPage();
?>