<?php
// functions.inc.php
class FormCreate {

	function startForm($action, $title)
	{
?>
	<div class="col-md-8">
	<form class="form-horizontal" action="<?php echo $action; ?>" method="post">
	<fieldset>
	<legend><?php echo $title; ?></legend>
<?php
	}
	function endForm()
	{
?>
	</fieldset>
	</form>
	</div>
<?php
	}

	function addField($type, $labelfor, $label, $name, $placeholder, $options = NULL, $value = NULL)
	{
		// Types : text, select, button, hidden
		if ($type == "text")
		{
?>
	<div class="form-group">
	  <label class="col-md-4 control-label" for="<?php echo $labelfor; ?>"><?php echo $label; ?></label>
	  <div class="col-md-4">
	    <input id="<?php echo $name; ?>" name="<?php echo $name; ?>" placeholder="<?php echo $placeholder; ?>" class="form-control input-md" value="<?php if (isset($value)) echo $value; ?>" />
	  </div>
	</div>
<?php
		}
		else if ($type == "textarea")
		{
?>
	<div class="form-group">
	  <label class="col-md-4 control-label" for="<?php echo $labelfor; ?>"><?php echo $label; ?></label>
	  <div class="col-md-4">
	    <textarea class="form-control" rows="5" name="<?php echo $name; ?>"></textarea>
	  </div>
	</div>
<?php			
		}
		else if ($type == "select")
		{
?>
	<div class="form-group">
	  <label class="col-md-4 control-label" for="<?php echo $labelfor; ?>"><?php echo $label; ?></label>
	  <div class="col-md-4">
	    <select id="<?php echo $name; ?>" name="<?php echo $name; ?>" class="form-control">
		<?php
			foreach ($options AS $option)
			{
				if ($option['id'] == $value)
					echo "<option value='".$option['id']."' selected=selected>".$option['name']."</option>";
				else
					echo "<option value='".$option['id']."'>".$option['name']."</option>";
			}
			?>      
	    </select>
	  </div>
	</div>
<?php
		}
		else if ($type == "button")
		{
?>
	<div class="form-group">
  	  <label class="col-md-4 control-label" for="singlebutton"></label>
  		<div class="col-md-4">
  		  <input type="submit" name="action" value="<?php echo $name; ?>" class="btn btn-primary">
  		</div>
  	</div>
<?php
		}
		else if ($type == "switch")
		{
?>
	<div class="form-group">
	  <label class="col-md-4 control-label" for="<?php echo $labelfor; ?>"><?php echo $label; ?></label>  
		<div class="col-md-4">
		  <input type="checkbox" id="<?php echo $name; ?>" value=1 name="<?php echo $name; ?>"<?php if (isset($value) AND $value == 1) echo " checked"; ?>>
		</div>
	</div>
<?php
		}
		else if ($type == "hidden")
		{
?>
	<input type="hidden" name="<?php echo $name; ?>" value="<?php echo $value; ?>">
<?php
		}
		else if ($type == "funkyradio")
		{
?>
		<div class="form-group">
		  <div class="funkyradio">
		  <div class="col-md-12"><p><?php echo $label; ?></p></div>
<?php			
			foreach ($options AS $option)
			{
?>	        <div class="col-md-3">
	          <div class="funkyradio-primary">
	            <input type="checkbox" name="<?php echo $name; ?>[<?php echo $option['id']; ?>]" id="<?php echo $labelfor . $option['id']; ?>" />
	            <label for="<?php echo $labelfor . $option['id']; ?>"><?php echo $option['name']; ?></label>
	          </div>
	        </div>
<?php
			}
?>
	      </div>
	    </div>
<?php			
		}
		else if ($type == "orderformitem")
		{
?>
		<div class="form-group">
  		  <div class="col-md-4">
  		  	
  		  </div>
		</div>
<?php
		}
	} 
}
function checkLogin()
{
	if (!isset($_COOKIE['USERID']))
	{
		header("Location: index.php");
		exit();
	}
}
?>