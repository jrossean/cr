<?php
// reportes.php by MJD
// This script is for adding, modifing and deactivating accounts
// include "index.php"; 
include "common.php";
require_once "includes/functions.inc.php";
require_once "includes/meekrodb.2.3.class.php";
$userid = $_COOKIE['USERID'];

if (isset($_REQUEST['option']))
  $option = $_REQUEST['option'];
else
  $option = "viewreports";



startPage("Reports");
//DB::debugMode();

?>
<div class="col-md-12">
  <a href="reports.php?option=transbycat" class="btn btn-primary">Transactions by Category</a>
  <a href="reports.php?option=transbyven" class="btn btn-primary">Transactions by Vendor</a>
  <a href="reports.php?option=transbyacct" class="btn btn-primary">Transactions by Account</a>
  <a href="reports.php?option=transbytype" class="btn btn-primary">Transactions by Account Type</a>

</div>
<br />



<?php


if (isset($option))
{
  if ($option == "transbycat")
  {
	$rows = db::query ("SELECT SUM(transactions.amount) transamount, COUNT(transactions.id) numtransactions, categorytype.name catname
	FROM transactions, categorytype
	WHERE transactions.`cattype` = categorytype.id
	AND transactions.userid = %i
	GROUP BY catname
	ORDER BY transamount DESC",$userid);

?>
<table id="transbycat" class="table table-bordered">
	<thead>
		<tr>
			<th>Category</th>
			<th># of Transactions</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
  
<?php

	foreach ($rows as $row){
?>
		<tr>
			<td><?php echo $row['catname']; ?></td>
			<td><?php echo $row['numtransactions']; ?></td>
			<td><?php echo $row['transamount']; ?></td>
		</tr>
<?php
	}

?>
	</tbody>
</table>
<?php
  }
  else if ($option =="transbyven"){
  	$rows = db::query ("SELECT SUM(transactions.amount) transamount, COUNT(transactions.id) numtransactions, vendorinfo.name venname
	FROM transactions, vendorinfo
	WHERE transactions.`vendorid` = vendorinfo.id
	AND transactions.userid = %i
	GROUP BY venname
	ORDER BY transamount DESC",$userid);

?>
<table id="transbyven" class="table table-bordered">
	<thead>
		<tr>
			<th>Vendor</th>
			<th># of Transactions</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
  
<?php

	foreach ($rows as $row){
?>
		<tr>
			<td><?php echo $row['venname']; ?></td>
			<td><?php echo $row['numtransactions']; ?></td>
			<td><?php echo $row['transamount']; ?></td>
		</tr>
<?php
	}

?>
	</tbody>
</table>
<?php
  }
    else if ($option =="transbyacct"){
  	$rows = db::query ("SELECT SUM(transactions.amount) transamount, COUNT(transactions.id) numtransactions, accounts.accountname acctname
	FROM transactions, accounts
	WHERE transactions.`accountid` = accounts.id
	AND transactions.userid = %i
	GROUP BY acctname
	ORDER BY transamount DESC",$userid);

?>
<table id="transbyven" class="table table-bordered">
	<thead>
		<tr>
			<th>Account</th>
			<th># of Transactions</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
  
<?php

	foreach ($rows as $row){
?>
		<tr>
			<td><?php echo $row['acctname']; ?></td>
			<td><?php echo $row['numtransactions']; ?></td>
			<td><?php echo $row['transamount']; ?></td>
		</tr>
<?php
	}

?>
	</tbody>
</table>
<?php
  }
	else if ($option =="transbytype"){
  	$rows = db::query ("SELECT SUM(transactions.amount) transamount, COUNT(transactions.id) numtransactions, accounts.accounttype accttype
	FROM transactions, accounts
	WHERE transactions.`accountid` = accounts.id
	AND transactions.userid = %i
	GROUP BY accttype
	ORDER BY transamount DESC",$userid);

?>
<table id="transbyven" class="table table-bordered">
	<thead>
		<tr>
			<th>Type of Account</th>
			<th># of Transactions</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
  
<?php

	foreach ($rows as $row){
?>
		<tr>
			<td><?php echo $row['accttype']; ?></td>
			<td><?php echo $row['numtransactions']; ?></td>
			<td><?php echo $row['transamount']; ?></td>
		</tr>
<?php
	}

?>
	</tbody>
</table>
<?php
  }
}
endPage();
?>
